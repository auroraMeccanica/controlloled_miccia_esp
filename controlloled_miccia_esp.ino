/*
Copyright © 2023 auroraMeccanica, Massimo Gismondi

This file is part of controlloled_miccia_esp
 
ontrolloled_miccia_esp is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
Vengono utilizzate due librerie esterne
- Esp8266 DMX command send library,
  Copyright © 2016,2021 Matthew Tong.
  License GNU GPL3 or later
  https://github.com/mtongnz/espDMX
- FastLED Copyright © 2013-2023 Daniel Garcia, Mark Kriegsman and other contributors.
  License MIT (expat)
  https://github.com/FastLED/FastLED
*/



//#define SERIAL

// Esce sul D4 il DMX
// Entra sul D6 il pulsante
// Esce sul D1 il LED

#include "led.h"

// Un numero più alto posticipa e rallenta il DMX
#define SFASAMENTO_SEC_AL_MINUTO 1.5
const float FATT_SFASAMENTO = (60.0-SFASAMENTO_SEC_AL_MINUTO)/60.0;


#define RITARDO_AVVIO 200

volatile static bool should_start_play = false;
volatile static bool is_playing = false;
#define PIN_BUTTON D6

#include "voce.h"
#include <espDMX.h>

#define CAMPIONI_RMS_AL_SECONDO 10
const float DURATA_CAMPIONE_RMS = 1000.0 / CAMPIONI_RMS_AL_SECONDO;

// Mantiene in ram i valori dei canali da inviare
// all'istante attuale
static byte valori_riga_ram[NUM_DMX_CHANNELS] = {0};

// Per evitare di accedere alla flash a ogni iterazione
// guardo se la lettura è necessaria
int last_access_index_computed = -1;

unsigned long start_millis = 0;

void pneumatic_drill_strobo(byte valori_riga_ram[NUM_DMX_CHANNELS], unsigned long time_passed);


// Le ISR vanno mantenute in RAM
ICACHE_RAM_ATTR void isr_start_play()
{
  should_start_play=true;
}

void setup()
{
  pinMode(PIN_BUTTON, INPUT_PULLUP);
  attachInterrupt(
    digitalPinToInterrupt(PIN_BUTTON),
    isr_start_play,
    FALLING);

  #ifdef SERIAL
    Serial.begin(9600);
  #endif

  
  // Esce sul D4
  dmxB.begin();

  dmxB.setChans(valori_riga_ram, NUM_DMX_CHANNELS);

  setup_leds();
}

void loop()
{
  // Se non sta suonando
  if (!is_playing)
  { 
    if (should_start_play)
    {
      // Ma deve iniziare a farlo

      // Spengo tutte le luci
      for (int i=0; i<NUM_DMX_CHANNELS; i++)
      {
        valori_riga_ram[i]=0;
      }
      dmxB.setChans(valori_riga_ram, NUM_DMX_CHANNELS);
      
      // Attendo 
      delay(RITARDO_AVVIO);
      start_millis = millis();
      should_start_play = false;
      is_playing = true;
    }
    else
    {
      // Né deve iniziare...

      // Accendo la luce di sala e spengo tutte le altre
      for (int i=0; i<NUM_DMX_CHANNELS; i++)
      {
        valori_riga_ram[i]=0;
      }
      valori_riga_ram[41-1] = 255;
      valori_riga_ram[45-1] = 255;
      dmxB.setChans(valori_riga_ram, NUM_DMX_CHANNELS);
      delay(2);
      return;
    }
  }
  int chosen_index;
  unsigned long time_passed = millis()-start_millis;
  time_passed *= FATT_SFASAMENTO;
  {
    recompute_led_strip(time_passed);

    float index_no_offset = time_passed / DURATA_CAMPIONE_RMS;
    
    // Tolgo 0.5 per ricentrare la media sul campione
    int tmp_index = floor(index_no_offset - 0.5);

    chosen_index = constrain(
      tmp_index,
      0,
      NUM_OF_SAMPLES // PERMETTO VOLONTARIAMENTE
      // Che vada fuori dall'array.
      // Se è così, smetto di inviare segnali dmx alla lampada
    );
  }
  if (chosen_index == NUM_OF_SAMPLES)
  {
    #ifdef SERIAL
      Serial.println("fine");
      Serial.println(millis()-start_millis);
    #endif
    // Non serve né trasferire memoria, né
    // inviare altri segnali dmx
    should_start_play=false;
    is_playing = false;
    return;
  }

  // Se non sto eseguendo nessuna sequenza
  // poiché è terminata,
  // oppure se non siamo ancora arrivati al
  // istante successivo,
  // evito di andare a leggere la memoria flash
  // ed esco prematuramente
  if (last_access_index_computed != chosen_index)
  {
    // Copia i valori dalla memoria flash
    memcpy_P(valori_riga_ram, DATI_DMX[chosen_index], NUM_DMX_CHANNELS);

    last_access_index_computed = chosen_index;
  } 

  pneumatic_drill_strobo(valori_riga_ram, time_passed);
  dmxB.setChans(valori_riga_ram, NUM_DMX_CHANNELS);
  delay(DURATA_CAMPIONE_RMS*0.02);
}


void pneumatic_drill_strobo(byte valori_riga_ram[NUM_DMX_CHANNELS], unsigned long time_passed)
{
  const unsigned long interv1_start = (60 + 18)*1000+500;
  const unsigned long interv1_end = (60 + 21)*1000;

  const unsigned long interv2_start = (60 + 21)*1000+500;
  const unsigned long interv2_end = (60 + 24)*1000+500;

  const unsigned long interv3_start = (60 + 25)*1000;
  const unsigned long interv3_end = (60 + 35)*1000+500;

  const unsigned long interv4_start = (60 + 36)*1000;
  const unsigned long interv4_end = (60 + 36)*1000+500;

  
  const byte DMX_ADDRESS_DRILL = 44;
  byte drill_index = DMX_ADDRESS_DRILL-1;
  
  if (
    time_passed > interv1_start && time_passed < interv1_end
    ||
    time_passed > interv2_start && time_passed < interv2_end
    ||
    time_passed > interv3_start && time_passed < interv3_end
    ||
    time_passed > interv4_start && time_passed < interv4_end
    )
  {
    unsigned int resto = time_passed % 200;
    if (resto>100)
    {
      valori_riga_ram[drill_index] = 255;
    }
    else
    {
      valori_riga_ram[drill_index] = 0;
    }
    
  }
}
