# Controllo Led miccia ESP

This project combines FastLED and EspDMX libraries to sync an audio player, an addressable LED strip and a predefined DMX command sequence.

# Copyright
This is **NOT** public domain, make sure to respect the license terms.

Copyright © 2023 auroraMeccanica, Massimo Gismondi

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses/.

## External libraries
This project includes some external libraries:
- Esp8266 DMX command send library,
  Copyright © 2016,2021 Matthew Tong.
  License GNU GPL3 or later
  <https://github.com/mtongnz/espDMX>
- FastLED library, Copyright © 2013-2023 Daniel Garcia, Mark Kriegsman and other contributors.
  License MIT (expat)
  <https://github.com/FastLED/FastLED>

