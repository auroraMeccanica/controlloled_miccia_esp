/*
Copyright © 2023 auroraMeccanica, Massimo Gismondi

This file is part of controlloled_miccia_esp
 
ontrolloled_miccia_esp is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "FastLED.h"

#define NUM_LEDS 130

CRGB leds[NUM_LEDS];

#define LED_PIN D1

void setup_leds()
{
  pinMode(D1, OUTPUT);
  FastLED.addLeds<WS2812B, LED_PIN, GRB>(leds, NUM_LEDS);

  for (int i=0; i<NUM_LEDS; i++)
  {
    leds[i] = CRGB(0,0,0);
  }
  FastLED.show();
}


void black_leds()
{
  for (int i=0; i<NUM_LEDS; i++)
  {
    leds[i] = CRGB(0,0,0);
  }
  FastLED.show();
}

float my_float_map(float x, float in_min, float in_max, float out_min, float out_max) {
  return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

float compute_wave(float v)
{
  if (v<-1.0 || v>1.0)
  {
    return 0.0;
  }

  if (v>0.9)
  {
    return my_float_map(v, 0.9, 1.0, 255.0, 0.0);
  }
  else
  {
    return my_float_map(v, -1.0, 0.9, 0.0, 255.0);
  }
  
  
  /*
  if (v>0.0)
  {
    return my_float_map(v, 0.0, 1.0, 255.0, 0.0);
  }
  else
  {
    return my_float_map(v, -1.0, 0.0, 0.0, 255.0);
  }
  */
}

void recompute_led_strip(unsigned long time_passed)
{
  
  // Inizio al secondo 21
  // Finisco al secondo 32-33
  const float WIDTH = 3000.0;
  //const long START_TIME = 23000;
  //const long END_TIME = 32000;
  const long START_TIME = 21000;
  const long END_TIME = 32000;
  const long LEN = END_TIME - START_TIME;


  if (time_passed < START_TIME
      ||
    time_passed > END_TIME  
  )
  {
    black_leds();
    return;
  }

  
  for (int i=0; i<NUM_LEDS; i++)
  {
    float cur_t = (float(LEN) / NUM_LEDS)*i + START_TIME;
    float k_t = ((float(time_passed) - START_TIME)/LEN)*(LEN + WIDTH) + START_TIME - WIDTH/2.0;

    float scaled_t = (cur_t - k_t)/(WIDTH/2.0);
    float res = compute_wave(scaled_t);
    leds[i] = CHSV(10,255, constrain(int(res), 0, 255));
  }
  
  FastLED.show();
}
